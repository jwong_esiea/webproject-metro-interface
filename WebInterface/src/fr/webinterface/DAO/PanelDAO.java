package fr.webinterface.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.webinterface.model.Case;
import fr.webinterface.model.Panel;
import fr.webinterface.model.User;

public class PanelDAO extends ConnexionDB {
	
	private final static Logger LOG = Logger.getLogger(PanelDAO.class);
	
	public PanelDAO() throws SQLException {
		super();
	}
	
	/**
	 * Cr�er la liste de bouton
	 * @param user
	 * @return
	 */
	public Panel findUserPanelByUser(User user){
		Panel panel = new Panel();
		List<Case> caseList = new ArrayList<Case>();
		try {
			Statement st = getConnexion().createStatement();
			st.execute("SELECT * FROM button WHERE idUser="+user.getIdUser()+";");
			ResultSet resultat = st.getResultSet();
			while (resultat.next()) {
				Case casetmp=this.getCase(Integer.parseInt(resultat.getString("idButton")));
				if(casetmp!=null)
					caseList.add(casetmp);
			}
			panel.setCaseList(caseList);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOG.error("Erreur findUser : " + e);
		}
		return panel;
	}
	/***
	 * Retourne le bouton
	 * @param idButton
	 * @return
	 */
	public Case getCase(int idButton){
		Case casePanel = null;
		try{
		Statement st = getConnexion().createStatement();
		st.execute("SELECT * FROM button WHERE idButton="+idButton+";");
		ResultSet resultat = st.getResultSet();
			while (resultat.next()) {
				casePanel = new Case(Integer.parseInt(resultat.getString("idButton")),resultat.getString("title"),resultat.getString("url"),Integer.parseInt(resultat.getString("size")),resultat.getString("backgroundColor"),resultat.getString("backgroundColorSelected"));
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		return casePanel;
	}
	/***
	 * Met a jour les boutons existant
	 * @param c
	 */
	public void updateCase(Case c) {
		try{
			PreparedStatement st = getConnexion().prepareStatement("UPDATE button SET title=?,url=?,size=?,backgroundColor=?,backgroundColorSelected=? WHERE idButton=?;");
			st.setString(1, c.getTitle());
			st.setString(2, c.getUrl());
			st.setInt(3, c.getFontsize());
			st.setString(4, c.getBackgroundClass());
			st.setString(5, c.getBackgroundSelectedClass());
			st.setInt(6, c.getIdCase());
			st.execute();
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
	/***
	 * Ajouter des boutons
	 * @param c
	 * @param p
	 */
	public void addCase(Case c,User u) {
		try{
			PreparedStatement stButtonInsert = getConnexion().prepareStatement("INSERT INTO button (title,url,idUser,size,backgroundColor,backgroundSelectedColor) values(?,?,?,?,?,?);",Statement.RETURN_GENERATED_KEYS);
			stButtonInsert.setString(1,c.getTitle());
			stButtonInsert.setString(2,c.getUrl());
			stButtonInsert.setInt(3, u.getIdUser());
			stButtonInsert.setInt(4, c.getFontsize());
			stButtonInsert.setInt(5, c.getFontsize());
			stButtonInsert.setInt(6, c.getFontsize());
			stButtonInsert.setInt(7, c.getFontsize());
			stButtonInsert.execute();
			stButtonInsert.getGeneratedKeys();	
		}
		catch(Exception e){
			System.out.println(e);
		}
		
	}
	
	public void deleteCase(Case c){
		try {
			PreparedStatement stButtonDelete = getConnexion().prepareStatement("DELETE FROM Button WHERE idButton=?");
			stButtonDelete.setInt(1, c.getIdCase());
			stButtonDelete.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

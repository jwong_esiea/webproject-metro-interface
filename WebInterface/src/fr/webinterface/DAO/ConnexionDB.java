package fr.webinterface.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionDB {
	private String url = "jdbc:mysql://localhost:3306/webinterface";
	private String user = "root";
	private String password = "";
	public Connection connexion;

	public ConnexionDB() throws SQLException{
		 try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connexion = DriverManager.getConnection(url,user,password);
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public Connection getConnexion(){
		return connexion;
	}
}
package fr.webinterface.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import fr.webinterface.model.Panel;
import fr.webinterface.model.User;

public class UserDAO extends ConnexionDB {

	private final static Logger LOG = Logger.getLogger(UserDAO.class);

	public UserDAO() throws SQLException {
		super();
	}

	public User findUserByLoginPassword(User userLogin) {
		User user = new User();
		try {
			Statement st = getConnexion().createStatement();
			st.execute("SELECT * FROM user where nom='" + userLogin.getName() + "'"+" AND " +"password="+"'" + userLogin.getPassword() + "';");
			ResultSet resultat = st.getResultSet();
			if (resultat.first()) {
				user.setIdUser(Integer.parseInt(resultat.getString("idUser")));
				user.setName(resultat.getString("nom"));
				user.setPassword(resultat.getString("password"));
				user.setEmail("email");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOG.error("Erreur findUser : " + e);
		}
		return user;

	}

	public User inscriptionUser(User user){
		
		try {
			PreparedStatement st = getConnexion().prepareStatement("INSERT INTO User (nom,password) values (?,?);");
			st.setString(1, user.getName());
			st.setString(2, user.getPassword());
			st.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}

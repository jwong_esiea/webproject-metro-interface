package fr.webinterface.model;

public class User {
	private int idUser;
	private String name;
	private String password;
	private String email;
	private Panel panel;

	/***
	 * 
	 */
	public User() {
		super();
	}

	/**
	 * @param idUser
	 * @param name
	 * @param password
	 * @param email
	 */
	public User(int idUser, String name, String password, String email,
			Panel panel) {
		this.idUser = idUser;
		this.name = name;
		this.password = password;
		this.email = email;
		this.panel = panel;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Panel getPanel() {
		return panel;
	}

	public void setPanel(Panel panel) {
		this.panel = panel;
	}

}

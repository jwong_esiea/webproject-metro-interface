package fr.webinterface.model;

public class Case {
	private int idCase;
	private String title;
	private String url;
	private int fontsize;
	private String backgroundClass;
	private String backgroundSelectedClass;
	
	/**

	 */
	public Case() {
		super();
	}

	/**
	 * @param idCase
	 * @param url
	 */
	public Case(int idCase,String title, String url) {
		super();
		this.idCase = idCase;
		this.title = title;
		this.url = url;
	}

	public Case(int idCase, String title, String url, int fontsize,
			String backgroundClass, String backgroundSelectedClass) {
		super();
		this.idCase = idCase;
		this.title = title;
		this.url = url;
		this.fontsize = fontsize;
		this.backgroundClass = backgroundClass;
		this.backgroundSelectedClass = backgroundSelectedClass;
	}

	public int getIdCase() {
		return idCase;
	}

	public void setIdCase(int idCase) {
		this.idCase = idCase;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getFontsize() {
		return fontsize;
	}

	public void setFontsize(int fontsize) {
		this.fontsize = fontsize;
	}

	public String getBackgroundClass() {
		return backgroundClass;
	}

	public void setBackgroundClass(String backgroundClass) {
		this.backgroundClass = backgroundClass;
	}

	public String getBackgroundSelectedClass() {
		return backgroundSelectedClass;
	}

	public void setBackgroundSelectedClass(String backgroundSelectedClass) {
		this.backgroundSelectedClass = backgroundSelectedClass;
	}
}

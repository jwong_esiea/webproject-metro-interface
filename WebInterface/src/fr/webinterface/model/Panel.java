package fr.webinterface.model;

import java.util.ArrayList;
import java.util.List;

public class Panel {
	private List<Case> caseList;

	/**
	 * @param caseList
	 */
	public Panel() {
		super();
	}

	/**
	 * @param caseList
	 */
	public Panel(List<Case> caseList) {
		this.caseList = new ArrayList<Case>();
	}

	public List<Case> getCaseList() {
		return caseList;
	}

	public void setCaseList(List<Case> caseList) {
		this.caseList = caseList;
	}

}

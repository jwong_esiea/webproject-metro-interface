package fr.webinterface.action;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import fr.webinterface.DAO.PanelDAO;
import fr.webinterface.model.Case;
import fr.webinterface.model.Panel;
import fr.webinterface.model.User;

@ManagedBean(name = "panelControl")
@RequestScoped
public class PanelControl {
	
	@ManagedProperty(value = "#{login.getUser()}")
	User user;
	Panel panel;
	PanelDAO panelPersistence;

	@PostConstruct
	public void init() {
		if(user!=null && user.getIdUser()!=0){
			try {
				panelPersistence = new PanelDAO();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			panel = panelPersistence.findUserPanelByUser(user);
			panel.getCaseList().add(new Case());
		}else{
			try {
				  final ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				  externalContext.redirect("./connexion.xhtml");
				    
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
	}

	public void addNewButton(){
		panel.getCaseList().add(new Case());
	}
	/***
	 * 
	 * @return
	 */
	public String editButton(){
		for (Case c : panel.getCaseList()) {
			if(c.getIdCase()!=0){
				panelPersistence.updateCase(c);
			}else {
				if(!c.getTitle().isEmpty() && !c.getUrl().isEmpty())
				panelPersistence.addCase(c,user);
			}
		}
		init();
		return "index.xhtml";
		
	}
	
	public String deleteButton(Case button){
		panelPersistence.deleteCase(button);
		this.init();
		return "Param_bouton.xhtml";
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Panel getPanel() {
		return panel;
	}

	public void setPanel(Panel panel) {
		this.panel = panel;
	}
}

package fr.webinterface.action;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import fr.webinterface.DAO.UserDAO;
import fr.webinterface.model.Panel;
import fr.webinterface.model.User;

@ManagedBean(name = "login")
@SessionScoped
public class Login {

	User user;
	String name;
	String mail;
	String password;
	String passwordVerification;
	UserDAO userPersistence;

	private final static Logger LOG = Logger.getLogger(Login.class);

	@PostConstruct
	public void init() {
		user = new User();
		try {
			userPersistence = new UserDAO();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String login() {
		if (user.getName().isEmpty() && user.getPassword().isEmpty()){
			user = new User();
		}else{ 
			user = userPersistence.findUserByLoginPassword(user);
			if(user.getIdUser() != 0){
				return "index.xhtml";
			}
		}
		return "connexion.xhtml";
	}

	public void inscription() {
		
		if (!name.isEmpty() && !mail.isEmpty() && !password.isEmpty()) {
			user = new User(0, name, password, mail, new Panel());
			User userinscription = new User();
			userinscription.setEmail(mail);
			userinscription.setName(name);
			userinscription.setPassword(password);
			userPersistence.inscriptionUser(userinscription);
		}
	}
	
	public String disconnect(){
	    final ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    externalContext.invalidateSession();
	    return "index.xhtml";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordVerification() {
		return passwordVerification;
	}

	public void setPasswordVerification(String passwordVerification) {
		this.passwordVerification = passwordVerification;
	}
}
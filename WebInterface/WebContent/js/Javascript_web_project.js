function init(){
	$('#connexion_modal').modal('show');
	$('#accueil-page').hide();
}

function push_button_connect()
{
	$('#connexion_modal').modal('hide');
	$('#accueil-page').show();
}

var dragSrcEl = null;

function dragStart(e) {
  // Target (this) element is the source node.
  this.style.opacity = '0.4';
  e.dataTransfer.setData("Text", ev.target.getAttribute('id'));
  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
  e.dataTransfer.setDragImage(e.target,0,0);
  return true;
}

function dragOver(e) {
    return false;
}
function dragDrop(e) {
   var src = e.dataTransfer.getData("Text");
   e.target.appendChild(document.getElementById(src));
   e.stopPropagation();
   return false;
}




var cols = document.querySelectorAll('#columns .column');
[].forEach.call(cols, function(col) {
  col.addEventListener('dragstart', handleDragStart, false);
  col.addEventListener('dragenter', handleDragEnter, false);
  col.addEventListener('dragover', handleDragOver, false);
  col.addEventListener('dragleave', handleDragLeave, false);
  col.addEventListener('drop', handleDrop, false);
  col.addEventListener('dragend', handleDragEnd, false);
});
//---------------------------------------------

function push_new_user()
{
	$('#connexion_modal').modal('hide');
	$('#new_user_modal').modal('show');
	
	validateinscription();
}

$("#pass").keyup(function() {

if ( count > 6 )
	{
		$("#info").css("backgroundColor","green"); 
		$("#info").css("color","white"); 
		$("#info").html("mot de passe superieur a 6 caracteres");
		 console.log("  mot de passe superieur a 6 caracteres  ");
		 var result= true;
	}
	else{
		 $("#info").css("backgroundColor","red"); 
		 $("#info").css("color","black"); 		 
		 $("#info").html("mot de passe inferieur a 6 caracteres");
		 console.log("  mot de passe inferieur a 6 caracteres  ");
		 $("#valid_button").attr("disabled","disabled");
		 var result= false;
	}
});


function push_button_valid_new_user()
{
	var count = $("#password_main").val().length;
	var count_valid= $("#password_valid").val().length;
	var result= false;
	
	if(count < 0 && count_valid == 0)
	{
	$("#info").css("backgroundColor","orange"); 
		$("#info").css("color","black"); 
	$("#info").html("Merci de valider votre mot de passe");
	console.log("  No valid : mot de passe  ");
	var result= false;
	}
	
	
	
	var valid_mdp_identical=$("#password_main").val();
	var mdp_identical=$("#password_main").val();
	
	if( mdp_identical.equals(valid_mdp_identical)==false)
	{
		$("#valid_button").attr("disabled","disabled");
		var result= false;
	}
	else
	{
		$("valid_button").attr("enable","enable");
	}
	
	return result;
}
//------------------------------------- Bouton Zone

$("#bouton2").mouseover(function(){
   $("#bouton2_zone_modif").show("slow");
   console.log("mouse enter");
 }).mouseleave(function() {
    $("#bouton2_zone_modif").hide();
	console.log("mouse leave");
  });

//----- Inscription Box ------/
/** V�rifie le nombre de lettre dans le password **/
function verifypasswordlength(){
	var length = $('#password_inscription').val().length;
	if(length> 6 ){
		$('#message_error_password').text("");
	}else{
		$('#message_error_password').text("Votre mot de passe est trop court");
		$('#message_error_password').css('color','red');
	}
}
/** Verifie que les deux que les deux passwords sont identiques**/
function verifytwopassword(){
	var password = $('#password_inscription').val();
	var passwordvalidation  = $('#password_valid_inscription').val();
	if(password == passwordvalidation){
		$('#message_error_password').text("");
	}else{
		$('#message_error_password').text("Les deux password ne sont pas identiques");
	}
}

/*** Active le bouton d'enregistrement si tout les champs sont rempli **/
function validateinscription(){
	var nom = $('#id_inscription').val();
	var email= $('#mail_inscription').val();
	var password = $('#password_inscription').val();
	var passwordvalidation = $('#password_valid_inscription').val();
	if((nom.length!=0) && (email.length!=0) && (password.length!=0) && (passwordvalidation.length!=0)){
		$('#valid_button').removeAttr("disabled");
	}
	else {
		$('#valid_button').attr('disabled','disabled');
	}
	
}
/*** Ferme la fenetre d inscription **/
function closeSubscriptionBox(){
	$('#new_user_modal').modal('hide');
	$('#connexion_modal').modal('show');
}

/**** Verifie si l adresse mail utilise un bon format ***/
function adresseMailFormat(){
	var mailregExp =  new RegExp("^([a-zA-Z0-9_\.\-])+@([a-zA-Z0-9\-])+.+([a-z]{2,4})+$");
	var mail = $("#mail_inscription").val();
	if(mail.match(mailregExp)){
		$("#email_message_subscription").remove();
	}else{
		if($("#email_message_subscription").length==0){
			var el = document.createElement("div");
			$(el).css("font-size","12px");
			$(el).css("color","red");
			$(el).attr("id","email_message_subscription");
			el.innerHTML="L'email entree est incorrecte";
			$("#mail_inscription").parent().append(el);
		}	
	}
}

/*** ****/
function showFormButton(currentButton){
	$(currentButton).parent().children("#formButton").toggle("slow");
}

function setExampleColor(currentButton){
	$(currentButton).parents("#formButton").find("#boutonExample").css("background-color",$(currentButton).val());
}
function changeSizeTextExample(currentButton){
	$(currentButton).parents("#formButton").find("#boutonExample").css("font-size",$(currentButton).val()+"px");
}

function setTextExample(currentButton){
	$(currentButton).parents("#formButton").find("#boutonText").html($(currentButton).val());
}

function setExampleSelectedColor(currentButton){
	$(currentButton).parents("#formButton").find("#boutonExample").removeClass().addClass("column " + $(currentButton).val());
}
-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           5.6.10 - MySQL Community Server (GPL)
-- Serveur OS:                   Win64
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for webinterface
DROP DATABASE IF EXISTS `webinterface`;
CREATE DATABASE IF NOT EXISTS `webinterface` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `webinterface`;


-- Dumping structure for table webinterface.button
DROP TABLE IF EXISTS `button`;
CREATE TABLE IF NOT EXISTS `button` (
  `idButton` int(10) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `url` varchar(150) NOT NULL,
  `size` int(10) NOT NULL DEFAULT '12',
  `backgroundColor` varchar(50) DEFAULT NULL,
  `backgroundColorSelectd` varchar(50) NOT NULL,
  PRIMARY KEY (`idButton`),
  KEY `FK_button_user` (`idUser`),
  CONSTRAINT `FK_button_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table webinterface.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
--- Creation Utilisateur 

CREATE USER  "webinterface" IDENTIFIED BY "progweb"
GRANT ALL PRIVILEGES ON `webinterface` . * TO 'webinterface'@'127.0.0.1';

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
